<?php
$name = $_GET['name'];
$type = $_GET['type'];
$currentPath = trim(shell_exec('pwd'));
error_log("name: ".$name." type: ".$type."\n", 3, $currentPath.'/php.log');
switch($type) {
	case 1:
		$append = '/xml_files/'.$name.'.xml';
		break;
	case 2:
		$append = '/xml_files/'.$name.'WithPos.xml';
		break;
	case 3:
		$append = '/slice_files/'.$name.'.slice.xml';
		break;
	case 4:
		$append = '/json_files/'.$name.'.json';
		break;	
	default:
		echo "Bad Download button";
}

$file = $currentPath.$append;

if(file_exists($file)) {
        header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
}

?>
