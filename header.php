<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Paulo Virote de Souza">

    <title>vizSlice</title>

    <link rel="icon" type="image/ico" href="img/logo2.png"/>
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
    <!--Special CSS if code has a light theme-->
    <?php
        if (isset($_COOKIE['syntaxHLTheme'])){
            
            if (strcmp($_COOKIE['syntaxHLTheme'],"default")==0 ||
                strcmp($_COOKIE['syntaxHLTheme'],"vs")==0 ||
                strcmp($_COOKIE['syntaxHLTheme'],"github")==0 ||
                strcmp($_COOKIE['syntaxHLTheme'],"color-brewer")==0){
                
                echo '<link href="css/highlightLight.css" rel="stylesheet">';
            }
            
        }
    
    ?>
    
    <!--FONTS-->
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <!--VISUALIZATIONS-->
    <script src="funcViz/adjListToMatrix.js"></script>
    <script src="https://d3js.org/d3.v3.min.js"></script>
    <!--<script src="funcViz/d3.stretched.chord.js"></script>-->
    <script src="funcViz/d3.layout.chord.sort.js"></script>
    <script src="funcViz/biPartite.js"></script>
    <!--<script src="https://syntagmatic.github.io/parallel-coordinates/d3.parcoords.js"></script>-->
    <script src="funcViz/d3.parcoords.js"></script>
    <script src="funcViz/encoder.js"></script>

    <!-- <link rel="stylesheet" type="text/css" href="https://syntagmatic.github.io/parallel-coordinates/d3.parcoords.css"> -->

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>

    <script type="text/javascript">

        var respondents = 16, //Total number of respondents (i.e. the number that makes up the total group)
                emptyPerc = .05, //What % of the circle should become empty
                emptyStroke = Math.round(respondents * emptyPerc);

        function changeVisible() {
            var e = document.getElementById("options");
            var viz = e.options[e.selectedIndex].value;
            document.getElementById("bipartite").style.display = "none";
            document.getElementById("parallel").style.display = "none";

            document.getElementById(viz).style.display = "inline-block";

        }
    </script>
    
    <!--Code Styles-->
    <?php 
        if (isset($_COOKIE['syntaxHLTheme'])){ 
            echo "<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/".$_COOKIE['syntaxHLTheme'].".min.css'>";}
        else{
            echo "<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/zenburn.min.css'>";}
    ?>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/highlight.min.js"></script>

    <?php

        // Read files from the directory json_files so it can be used where it is needed
        // This function reads all the files from the specified folder and removes the extension
        // considering that it has 3 or 4 characters after the '.'. After that it removes the files
        // '..', '.' and '.DS_Store' (that is created by macs) if they are in the array 
        function scanDirectory($directoryJSON){
            $scanned_directory = array_diff(preg_replace('/\\.[^.\\s]{3,4}$/', '', 
                                            scandir($directoryJSON)), 
                                            array('..', '.', '.DS_Store'));
            return $scanned_directory;
        }

        if (!isset($_COOKIE['filesList']) || 
             sizeof(scanDirectory("json_files/")) != sizeof(unserialize($_COOKIE['filesList']))){
            
            setcookie("filesList", serialize(scanDirectory("json_files/")), time()+360000);

            // Reload page to display new file
            echo "<script> window.location.reload();</script>";
        }
        else{
            setcookie("filesList", $_COOKIE['filesList'], time()+360000);
        }
        

        // In order to access the files from the COOKIE we just set, you have to unserialize it
        // so the array is formed again, then you can use a foreach to go through all the files

        // $filesList = unserialize($_COOKIE['filesList']);
        // foreach ($filesList as $key => $value) {
        //    // Your code goes here
        //    echo $value . "<br>";
        // }
    ?>
    
</head>