<?php
session_start();

if (!isset($_SESSION['filename'])){
    header( "Location: index.php" );
}
else{
    $_SESSION['currentLocation'] = $_SERVER['REQUEST_URI'];
    $_SESSION['fullURL'] = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
}

?>

<!DOCTYPE html>
<html lang="en">

<?php include "header.php"; ?>
    
<body>
    <?php include "topNavBar.php"; ?>
    
    <div id="wrapper" class="toggled">  <!--class="toggled"-->
        
        <?php include "leftBar.php"; ?>
        
        <!-- Page Content -->
        <div id="page-content-wrapper" class="navigationPage">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="fontTitle">
                            <span class="fa-stack">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-file-code-o fa-stack-1x fa-inverse"></i>
                            </span>
                            <span id="directoryCode">
                                <?php echo $_SESSION['filename']; ?>
                            </span>
                        </h1>
                        
                        <h1 class="selectorVisualization">
                            <select onchange="changeVisible()" id="options">
                                <option value="bipartite" selected="selected">Bipartite</option>
                                <option value="parallel">Parallel</option>
                            </select>
                        </h1>
                        
                        
                        <hr>        
                        <div class="row rowVisualizations" id="displayChart">
                          <div class="col-lg-12">
                                <div class="row">
                                    <div id="bipartite" class="viz col-sm-12 col-lg-6"></div>
                                    <div id="parallel" class="viz col-sm-12 col-lg-6" style="display:none">
                                        <div class="parcoords"> </div>
                                    </div>

                                    <!-- <div class="viz col-sm-12 col-lg-6" id="parallel"></div> -->
                                    
                                    <div class="code col-sm-12 col-lg-5">
                                        <div class="code2 col-sm-4 col-lg-3" id="sourceMini">
                                            <table id="contextView">
                                                <tbody style="display:none"></tbody>
                                            </table>
                                        </div>
                                        <pre><code id="source" class="cpp"></code></pre>
                                    </div>
                                    
                                </div>
                          </div><!--/col-12-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
        
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    
    <?php 
        include "modalWindows.php"; 
        include "includeScripts.php";
        include "includeScriptsVisualizations.php";
        include "mapScripts.php";
    ?>
    
    <script>
        $(".code pre code, .code2").css("max-height", $(window).height()-200);
        
        $('#treeMap').on('shown.bs.modal', function() {
            fixLegend();
            goThroughMap($("#directoryCode").text());
        });

    </script>
    
</body>
</html>
