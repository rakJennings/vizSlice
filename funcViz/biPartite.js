!function(){
	var bP={};
	var b=30, bb=150, height=500, buffMargin=1, minHeight=10;
	var c1=[-130, 40], c2=[-50, 100], c3=[-10, 140]; //Column positions of labels.
	var colors = d3.scale.category20b();
	bP.selected=-1;
	bP.selectedP=-1;
	bP.partData = function(data){
		var sData={};

		sData.keys=[
			d3.set(data.map(function(d){ return d[0];})).values().sort(function(a,b){ return ( a<b? -1 : a>b ? 1 : 0);}),
			d3.set(data.map(function(d){ return d[1];})).values().sort(function(a,b){ var x = parseInt(a); var y = parseInt(b); return x-y;})
		];

		sData.data = [	sData.keys[0].map( function(d){ return sData.keys[1].map( function(v){ return 0; }); }),
						sData.keys[1].map( function(d){ return sData.keys[0].map( function(v){ return 0; }); })
		];

		data.forEach(function(d){
			sData.data[0][sData.keys[0].indexOf(d[0])][sData.keys[1].indexOf(d[1])]=1;
			sData.data[1][sData.keys[1].indexOf(d[1])][sData.keys[0].indexOf(d[0])]=1;
		});

		return sData;
	}

	function visualize(data){
		var vis ={};
		function calculatePosition(a, s, e, b, m, sub){
			var total=d3.sum(a);
			var sum=0, neededHeight=0, leftoverHeight= e-s-2*b*a.length;
			var ret =[];
			a.forEach(
				function(d){
					var v = {};
					v.value = d;
					v.percent = (total == 0 ? 0 : d / total);
					v.height = d * m;
					(v.height == m ? leftoverHeight -= m : neededHeight += v.height );
					ret.push(v);
				}
			);

			var scaleFact=leftoverHeight/Math.max(neededHeight,1), sum=0;
			if(sub) {
				ret.forEach(
					function (d) {
						d.h = d.value * m;
						d.middle = sum + b + d.h / 2;
						var top = sum+b;
						d.y = s + top;
						sum += 2 * b + d.h;
					}
				);
			}else{
				ret.forEach(
					function (d) {
						d.height = d.value * m;
						d.middle = sum + b + d.height / 2;
						var top = sum+b;
						d.y = s + top;
						sum += 2 * b + d.height;
					}
				);
			}
			return ret;
		}

		vis.mainBars = [
			calculatePosition( data.data[0].map(function(d){ return d3.sum(d);}), 0, height, buffMargin, minHeight, false),
			calculatePosition( data.data[1].map(function(d){ return d3.sum(d);}), 0, height, buffMargin, minHeight, false)
		];

		vis.subBars = [[],[]];
		vis.mainBars.forEach(function(pos,p){
			pos.forEach(function(bar, i){
				calculatePosition(data.data[p][i], bar.y, bar.y+bar.h, 0, minHeight, true).forEach(function(sBar,j){
					sBar.key1=(p==0 ? i : j);
					sBar.key2=(p==0 ? j : i);
					vis.subBars[p].push(sBar);
				});
			});
		});

		vis.subBars.forEach(function(sBar){
			sBar.sort(function(a,b){
				return (a.key1 < b.key1 ? -1 : a.key1 > b.key1 ?
						1 : a.key2 < b.key2 ? -1 : a.key2 > b.key2 ? 1: 0 )});
		});

		vis.edges = vis.subBars[0].map(function(p,i){
			return {
				key1: p.key1,
				key2: p.key2,
				y1:p.y,
				y2:vis.subBars[1][i].y,
				h1:p.h,
				h2:vis.subBars[1][i].h
			};
		});
		vis.keys=data.keys;
		return vis;
	}

	function arcTween(a) {
		var i = d3.interpolate(this._current, a);
		this._current = i(0);
		return function(t) {
			return edgePolygon(i(t));
		};
	}
    var x=-1;
	function drawPart(data, id, p){
		d3.select("#"+id).append("g").attr("class","part"+p)
			.attr("transform","translate("+( p*(bb+b))+",0)");
		d3.select("#"+id).select(".part"+p).append("g").attr("class","subbars");
		d3.select("#"+id).select(".part"+p).append("g").attr("class","mainbars");

		var mainbar = d3.select("#"+id).select(".part"+p).select(".mainbars")
			.selectAll(".mainbar").data(data.mainBars[p])
			.enter().append("g").attr("class","mainbar");
        
        x++;
		mainbar.append("rect").attr("class","mainrect mainrectOpacity")
			.attr("x", 0).attr("y",function(d){ return d.middle-d.height/2; })
			.attr("width",b).attr("height",function(d){return d.height; })
			.style("shape-rendering","auto")
			
        // document.get
        
        /*IF P==1 THE LABELS ARE FOR THE LINE NUMBERS*/
        if (p == 0){
            mainbar.append("text").attr("class","barlabel varNameLabel")
                .attr("x", c1[p]).attr("y",function(d){ return d.middle+5;})
                .text(function(d,i){ return data.keys[p][i];})
                .attr("text-anchor","start" );
        }
        if (p == 1){
            mainbar.append("text").attr("class","barlabel lineNumberLabel")
                .attr("x", c1[p]-5).attr("y",function(d){ return d.middle+5;})
                .text(function(d,i){ return data.keys[p][i];})
                .attr("text-anchor","start" );
        }

		mainbar.append("text").attr("class","barvalue")
			.attr("x", c2[p]).attr("y",function(d){ return d.middle+5;})
			.text(function(d,i){ return d.value ;})
			.attr("text-anchor","end");

		/*mainbar.append("text").attr("class","barpercent")
			.attr("x", c3[p]).attr("y",function(d){ return d.middle+5;})
			.text(function(d,i){ return "( "+Math.round(100*d.percent)+"%)" ;})
			.attr("text-anchor","end").style("fill","grey");*/

		d3.select("#"+id).select(".part"+p).select(".subbars")
			.selectAll(".subbar").data(data.subBars[p]).enter()
			.append("rect").attr("class","subbar")
			.attr("x", 0).attr("y",function(d){ return d.y})
			.attr("width",b).attr("height",function(d){ return (d.h)})
			.style("fill",function(d){ return colors(d.key1);});
			//.style("visibility", function(d){return d.visible});
	}

	function drawEdges(data, id){
		d3.select("#"+id).append("g").attr("class","edges").attr("transform","translate("+ b+",0)");

		d3.select("#"+id).select(".edges").selectAll(".edge")
			.data(data.edges).enter().append("polygon").attr("class","edge")
			.attr("points", edgePolygon).style("fill",function(d){ return colors(d.key1);})
			.style("opacity",0.5).each(function(d) { this._current = d; });
	}

	function drawHeader(header, id){
		d3.select("#"+id).append("g").attr("class","header").append("text").text(header[2])
			.style("font-size","20").attr("x",108).attr("y",-20).style("text-anchor","middle")
			.style("font-weight","bold");

		[0,1].forEach(function(d){
			var h = d3.select("#"+id).select(".part"+d).append("g").attr("class","header");

			h.append("text").text(header[d]).attr("x", (c1[d]-5))
				.attr("y", -5).style("fill","grey");

			h.append("text").text("Count").attr("x", (c2[d]-10))
				.attr("y", -5).style("fill","grey");

			h.append("line").attr("x1",c1[d]-10).attr("y1", -2)
				.attr("x2",c3[d]+10).attr("y2", -2).style("stroke","black")
				.style("stroke-width","1").style("shape-rendering","crispEdges");
		});
	}

	function edgePolygon(d){
		return [0, d.y1, bb, d.y2, bb, d.y2+d.h2, 0, d.y1+d.h1].join(" ");
	}

	function transitionPart(data, id, p){
		var mainbar = d3.select("#"+id).select(".part"+p).select(".mainbars")
			.selectAll(".mainbar").data(data.mainBars[p]);

		mainbar.select(".mainrect").transition().duration(500)
			.attr("y",function(d){ return d.y;})
			.attr("height",function(d){ return d.height;});

		mainbar.select(".barlabel").transition().duration(500)
			.attr("y",function(d){ return d.middle+5;});

		mainbar.select(".barvalue").transition().duration(500)
			.attr("y",function(d){ return d.middle+5;}).text(function(d,i){ return d.value ;});

		/*mainbar.select(".barpercent").transition().duration(500)
			.attr("y",function(d){ return d.middle+5;})
			.text(function(d,i){ return "( "+Math.round(100*d.percent)+"%)" ;});*/

		d3.select("#"+id).select(".part"+p).select(".subbars")
			.selectAll(".subbar").data(data.subBars[p])
			.transition().duration(500)
			.attr("y",function(d){ return d.y}).attr("height",function(d){ return d.h});
			//.style("visibility", function(d){return d.visible});

	}

	function transitionEdges(data, id){
		d3.select("#"+id).append("g").attr("class","edges")
			.attr("transform","translate("+ b+",0)");

		d3.select("#"+id).select(".edges").selectAll(".edge").data(data.edges)
			.transition().duration(500)
			.attrTween("points", arcTween)
			.style("opacity",function(d){ return (d.h1 ==0 || d.h2 == 0 ? 0 : 0.5);});
	}

	function transition(data, id){
		transitionPart(data, id, 0);
		transitionPart(data, id, 1);
		transitionEdges(data, id);
	}

	bP.draw = function(data, svg){
		var max = Math.max(data[0].data.keys[0].length, data[0].data.keys[1].length);
		height = max*minHeight+max*buffMargin;

		data.forEach(function(biP,s){
			svg.append("g")
				.attr("id", biP.id)
				.attr("transform","translate("+ (550*s)+",0)");
			var visData = visualize(biP.data);
			drawPart(visData, biP.id, 0);
			drawPart(visData, biP.id, 1);
			drawEdges(visData, biP.id);
			drawHeader(biP.header, biP.id);

			[0,1].forEach(function(p){
				d3.select("#"+biP.id)
					.select(".part"+p)
					.select(".mainbars")
					.selectAll(".mainbar")
					//.on("mouseover",bP.fade(0,biP.id, p))
					.on("click",bP.fade(.5,biP.id, p));
			});
		});
	};

	/*bP.selectSegment = function(data, m, s){
		data.forEach(function(k){
			var newdata =  {keys:[], data:[]};

			newdata.keys = k.data.keys.map( function(d){ return d;});

			newdata.data[m] = k.data.data[m].map( function(d){ return d;});

			newdata.data[1-m] = k.data.data[1-m]
				.map( function(v){ return v.map(function(d, i){ return (s==i ? d : 0);}); });

			transition(visualize(newdata), k.id);

			var selectedBar = d3.select("#"+k.id).select(".part"+m).select(".mainbars")
				.selectAll(".mainbar").filter(function(d,i){ return (i==s);});

			selectedBar.select(".mainrect").style("stroke-opacity",1);
			selectedBar.select(".barlabel").style('font-weight','bold');
			selectedBar.select(".barvalue").style('font-weight','bold');
			//selectedBar.select(".barpercent").style('font-weight','bold');
		});


	};

	bP.deSelectSegment = function(data, m, s){
		data.forEach(function(k){
			transition(visualize(k.data), k.id);

			var selectedBar = d3.select("#"+k.id).select(".part"+m).select(".mainbars")
				.selectAll(".mainbar").filter(function(d,i){ return (i==s);});

			selectedBar.select(".mainrect").style("stroke-opacity",0);
			selectedBar.select(".barlabel").style('font-weight','normal');
			selectedBar.select(".barvalue").style('font-weight','normal');
			//selectedBar.select(".barpercent").style('font-weight','normal');
		});
	};*/

	bP.fade = function fade(opacity, id, p) {
		return function (d, i){
			var edges = d3.select("#"+id).select(".edges").selectAll(".edge");
			if(bP.selected==i && bP.selectedP == p) {
				edges.transition().style("opacity", opacity);
				bP.selected = -1;
				bP.selectedP = -1;
				
				$(".bipartiteSelected").removeClass("bipartiteSelected");
				clearCodeHighlight();

			} else {
				bP.selected = i;
				bP.selectedP = p;
				//fade out
				var tmp = edges.filter(function (d) {
					if(p==0){
						return d.key1 != i;
					} else {
						return d.key2 != i;
					}
				});

				tmp.transition().style("opacity", 0);
				//fade in
				tmp = edges.filter(function (d) {
					if(p==0){
						return d.key1 == i;
					} else {
						return d.key2 == i;
					}
				});
				tmp.transition().style("opacity", opacity);
			}
		};
	};
	this.bP = bP;
}();
