/**
 * Created by kaczkara on 1/14/16.
 */
function makeMatrix(func_path, data){
    var path = func_path.split("/");
    var i =0;
    if (data.name == path[i]) {
        i++;
    }
    for ( i; i < path.length; i++) {
        for (var j=0; j< data.children.length; j++){
            if (data.children[j].name == path[i]){
                data = data.children[j];
                break;
            }
        }
    }

    /*var hasBegin = data.children[0]['BEGIN_LINE'] != -1;
    if (hasBegin){
        //var begin = data.children[0]['BEGIN_LINE'];
        //var end = data.children[0]['END_LINE'];
        //var funcSize =  end - begin + 1;
        //var size = funcSize + 2 + data.children.length;
        //var matrix = new Array(size);
        //var labels = new Array(size);
        var bp = [];
        var para = [];
        /*for (var i = 0; i < size; i++){
            matrix[i] = new Array(size).fill(0);
            labels[i] = begin + i;
        }*/
        //labels[funcSize] = "";
        /*for (var i = 0; i < data.children.length; i++){
            var sl = data.children[i]["SLICING_LINES"];
            //var varPos = funcSize+1+i;
            var varName = data.children[i].name;
            labels[varPos] = varName;
            for (var j = 0; j < sl.length; j++){
                var line = sl[j];
                //var linePos= labels.indexOf(line);
                //console.log(begin, end);
                //console.log(line, linePos, varPos);
                //matrix[linePos][varPos] = 1;
                //matrix[varPos][linePos] = 1;
                bp.push([varName, line.toString(), 1]);
                para.push({"line":line, "varID":i, "name":varName});
            }
        }
        var bl2 = labels.length - 1;
        var bl1 = labels.indexOf("");
        labels[bl2] = "";
        matrix[bl2][bl1] = emptyStroke;
        matrix[bl1][bl2] = emptyStroke;
        var obj = {"labels": labels, "matrix":matrix, "bipartite":bp, "parallel":para};
        return obj;*/
    //} else {
        var labels = [];
        var lines = [];
        var bp = [];
        var para = [];
        for (var i =0; i < data.children.length; i++){
            labels.push(data.children[i].name);
            var sl = data.children[i]["SLICING_LINES"];
            for (var j = 0; j < sl.length; j++) {
                if (lines.indexOf(sl[j]) == -1) {
                    lines.push(sl[j]);
                }
            }
        }

        //var size = labels.length + lines.length + 2;
        //var matrix = new Array(size);
        /*for (var i = 0; i < size; i++){
            matrix[i] = new Array(size).fill(0);
        }*/
        lines.sort();
        labels = lines.concat([""]).concat(labels);

        for (var i =0; i < data.children.length; i++){
            var name = data.children[i].name;
            //var varPos = labels.indexOf(name);
            var sl = data.children[i]["SLICING_LINES"];
            for (var j = 0; j < sl.length; j++){
                var line = sl[j];
                //var linePos= labels.indexOf(line);
                //console.log(line, linePos, varPos);
                //matrix[linePos][varPos] = 1;
                //matrix[varPos][linePos] = 1;
                bp.push([name, line.toString(), 1]);
                para.push({"variable":name, "line":line.toString()});
            }
        }

        //var bl2 = labels.length;
        //var bl1 = labels.indexOf("");
        //labels.push("");
        //matrix[bl2][bl1] = emptyStroke;
        //matrix[bl1][bl2] = emptyStroke;
        var obj = {"labels": labels, "bipartite":bp, "parallel":para};
        return obj;
    //}

}
