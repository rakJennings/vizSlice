<script src="funcViz/script.js"></script>
<script type="text/javascript">
    
    //Update Title to the location
    <?php
        if (isset($_SESSION['currentLocation'])){
            echo "var data ='".$_SESSION['currentLocation']."';";
            echo "var arr = data.split('=');";
            echo "arr = arr[1].split('&');";
            echo "$('#directoryCode').text(arr[0]);";
        }
    ?>
    
    
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function drawBP(bpData) {
        var data = [{data: bP.partData(bpData), id: 'Slicing', header: ["Variable", "Sline", ""]}];
        var margin = {b: 0, t: 40, l: 140, r: 0};
        //console.log(height);
        var svg = d3.select("#bipartite")
                .append("svg")
                .append("g").attr("transform", "translate(" + margin.l + "," + 15 + ")").attr("id", "main");
        bP.draw(data, svg);

        //adjust height & width
        var bbox = d3.select("#Slicing")[0][0].getBBox();
        d3.select("#bipartite").select("svg").attr('height', bbox.height).attr('width', bbox.width);
    }

    function drawParallel2(data){
        /*var colorgen = d3.scale.ordinal()
        .range(["#a6cee3","#1f78b4","#b2df8a","#33a02c",
            "#fb9a99","#e31a1c","#fdbf6f","#ff7f00",
            "#cab2d6","#6a3d9a","#ffff99","#b15928"]);
        */
        var colorgen = d3.scale.category20b();
        var color = function(d) { return colorgen(d.variable); };
        console.log(data);
        var parcoords = d3.parcoords()(".parcoords")
            .data(data)
            .color(color)
            .alpha(0.7)
            .composite("darken")
            .render()
            .brushMode("1D-axes");
        parcoords.flipAxes(['line']);
        parcoords.alphaOnBrushed(.3);

        parcoords.svg.selectAll("text")
            .style("font", "12px sans-serif");
        tmp = parcoords;
    }

    function loadSrc(path){
        d3.xml(getParameterByName("xml"), function(error, d){
            //console.log(path);
            var splitP = path.split('/');
            var func = splitP[splitP.length-1];
            var file = splitP[0];
            for (var i =1;i<splitP.length-1;i++){
                file=file+"/"+splitP[i];
            }
            //console.log(file, func);
            var searchParam = "unit[filename='"+ file +"']";
            var fileData = $(d).find(searchParam);
            var t = '';
            if(func !="[GLOBAL-VARIABLES]"){
                t = $(fileData).find('function:contains('+func+')')[0].innerHTML.replace(/(<([^>]+)>)/ig,"");
            } else {
                t = fileData[0].innerHTML.replace(/(<([^>]+)>)/ig,"");
            }
            t=Encoder.htmlDecode(t);
            d3.select("#source").text(t);
            hljs.initHighlighting();
            mapCode();
            addLineNumbers();
            
        });
    }
    function JSON2CSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var line = '';
        var head = array[0];
        /*if ($("#quote").is(':checked')) {
            for (var index in array[0]) {
                var value = index + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {*/
            for (var index in array[0]) {
                line += index + ',';
            }
        /*}*/

        line = line.slice(0, -1);
        str += line + '\r\n';

        for (var i = 0; i < array.length; i++) {
        var line = '';
        /*if ($("#quote").is(':checked')) {
            for (var index in array[i]) {
                var value = array[i][index] + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {*/
            for (var index in array[i]) {
                line += array[i][index] + ',';
            }
        //}

        line = line.slice(0, -1);
        str += line + '\r\n';
        }
        return str;    
    }
    
    var tmp;
    var global_bpData;
    d3.json(getParameterByName('data'), function (root) {
        var path = getParameterByName('path');
        var data = makeMatrix(path, root[0]);
        //var Names = data.labels;
        //var matrix = data.matrix;
        var bpData = data.bipartite;
        tmp = data.labels;
        global_bpData = bpData;
        //respondents = Names.length; //Total number of respondents (i.e. the number that makes up the total group)
        //emptyStroke = Math.round(respondents * emptyPerc);
        drawBP(bpData);
        //displayChart(Names, matrix);
        drawParallel2(data.parallel);
        changeVisible();
        loadSrc(path);
        /*var file = data.parallel;
        var blob = new Blob([JSON2CSV(file)], {type: "octet/stream"});
        var blobURL = URL.createObjectURL(blob);
        var pom = document.createElement('a');
            pom.setAttribute('href', blobURL);
            pom.setAttribute('download', "data.csv");
            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            } else {
                pom.click();
            }*/

    });
    
    
    function highlightRow(selected){
        
        var numId = selected.split("-");
        
        if (!$("td[title*='codeLine-"+numId[1]+"']").hasClass("highlightCode")){
            clearCodeHighlight();
            $(".codeLine-"+numId[1]).addClass("highlightCode");
            $(".line-"+numId[1]).addClass("highlightNumber");
        }
        else{
            clearCodeHighlight();
        }
    }
    
    function replaceMetas( str ) {
        return str.replace(/["~!#$%^&*\(\)+=`{}\[\]\|\\:;'<>,.\/?"\-]+/g, ' '); //@ _ \t\r\n
    }
    
    //TURN CODE INTO A TABLE
    function mapCode(){        
        var pre = document.getElementsByTagName('code');
        var codeLines = pre[0].innerHTML.split(/\n/);
        $('pre code').html('<table>'+$.map($('pre').text().split('\n'), function(t, i){
            return "<tr><td></td><td>"+codeLines[i]+"</td></tr>"; //onclick='highlightRow(this.title)'
        }).join('')+'</table>');
        fixComments(codeLines);
    }
    
    function fixComments(codeLines){
        var comment=false;
        $("pre code table td:last-child").each (function(indexIn){
            var lineText = $(this).text();

            if (lineText.search("\\/\\*")>-1 && lineText.search("\\*\\/")<0){ //-1 == not found
                comment=true;}
            
            //Check if there already is a comment
            if (!(codeLines[indexIn].indexOf("hljs-comment")>=0) && comment == true){
                $(this).wrapInner("<span class='hljs-comment'></span>");}
            
            //Check if there is a comment ending
            if(lineText.search("\\/\\*")<0 && lineText.search("\\*\\/")>-1){
                comment=false;}
        });
    }
    //Implemented by Paulo de Souza
    
    //Clear current highlighting in code if any
    function clearCodeHighlight(){
        $("pre code table td:last-child").removeClass("highlightCode");
        $("#contextView td:last-child").removeClass("highlightCode");
        $("pre code table td:first-child").removeClass("highlightNumber");
        $("#contextView td:first-child").removeClass("highlightNumber");
    }
    
    //Add border around bipartite variables and lines and then highlight code
    function highlightCodeAndBipartite(varName){
        clearCodeHighlight();
        var total = global_bpData.length;
        var numOfVars = $(".varNameLabel").length;
        
        //IF LINE SIDE SELECTED
        if ($.isNumeric(varName)){
            for (i=0;i<total;i++){
                //alert(global_bpData[i][0]);
                if (varName == global_bpData[i][1]){
                    var varNameBipartite = global_bpData[i][0];
                    
                    $(".mainrect").each (function(indexIn){
                        if(numOfVars>indexIn){
                            if($(".varNameLabel:eq("+indexIn+")").text() == varNameBipartite){
                                this.classList.add("bipartiteSelected");
                                //return false;
                            }
                        }
                    });
                    $("pre code table td:last-child[title='codeLine-"+varName+"']").addClass("highlightCode");
                    $("pre code table td:first-child[title='line-"+varName+"']").addClass("highlightNumber");
                    $("#contextView td:last-child[title='codeLine-"+varName+"']").addClass("highlightCode");
                    $("#contextView td:first-child[title='line-"+varName+"']").addClass("highlightNumber");
                    
                    //break;
                }
            }
        }
        //IF VAR SIDE IS SELECTED
        else{
            //alert(numOfVars);
            for (i=0;i<total;i++){
                
                if (varName == global_bpData[i][0]){
                    var sline = global_bpData[i][1];
                    
                    $(".mainrect").each (function(indexIn){
                        if(numOfVars<=indexIn){
                            var slineLabelEq = indexIn-numOfVars;
                            var lineLabel = $(".lineNumberLabel:eq("+slineLabelEq+")").text();
                            if(lineLabel == sline){
                                this.classList.add("bipartiteSelected");
                                $("pre code table td:last-child[title='codeLine-"+sline+"']").addClass("highlightCode");
                                $("pre code table td:first-child[title='line-"+sline+"']").addClass("highlightNumber");
                                $("#contextView td:last-child[title='codeLine-"+sline+"']").addClass("highlightCode");
                                $("#contextView td:first-child[title='line-"+sline+"']").addClass("highlightNumber");
                            }
                        }
                    }); 
                    //break;
                }
            }
        }
        
        //alert(global_bpData);
    }

    //Add border around bipartite variables and lines and then highlight code
    function highlightCodeFromParallel(varName){
        var total = global_bpData.length;
        var numOfVars = $(".varNameLabel").length;
        
        //alert(numOfVars);
        for (i=0;i<total;i++){
            //alert(global_bpData[i][0]);
            if (varName == global_bpData[i][1]){
                $("pre code table td:last-child[title='codeLine-"+varName+"']").addClass("highlightCode");
                $("pre code table td:first-child[title='line-"+varName+"']").addClass("highlightNumber");
                $("#contextView td:last-child[title='codeLine-"+varName+"']").addClass("highlightCode");
                $("#contextView td:first-child[title='line-"+varName+"']").addClass("highlightNumber");
                
                break;
            }
        }
        
        //alert(global_bpData);
    }
    
    //Global variable that stores the current highlighted variable/line
    var currentHighlight;
    
    //When you click the bipartite, highlights the code
    $("#bipartite").on("click", ".mainrect",function(){
        clearCodeHighlight();
        var numOfVars = $(".varNameLabel").length;
        var indexRect = $(this).closest('.mainrect').index('.mainrect');
        var varName;
        var lineOrVar; //true for line, false for var
        
        var total = global_bpData.length;
        
        firstFound=false;
        
        //Gets var name from bipartite according to rectangle clicked
        if(indexRect<numOfVars){
            varName = $(".varNameLabel:eq("+indexRect+")").text();
            lineOrVar=false;
            
        }else if(indexRect>=numOfVars){
            indexRect = indexRect-numOfVars;
            varName = $(".lineNumberLabel:eq("+indexRect+")").text();
            lineOrVar=true;
        }
        //$(".lineNumberLabel:eq("+indexRect+")").attr("class", "bipartiteSelected");
        
        //REMOVE HIGHLIGHT FROM BIPARTITE
        $(".bipartiteSelected").removeClass("bipartiteSelected");

        
        if(currentHighlight!=varName){
            currentHighlight = varName;    
            //BORDER AROUND RECTANGLE
            this.classList.add("bipartiteSelected");
            highlightCodeAndBipartite(varName);

        } else{
            clearCodeHighlight();
            currentHighlight="";
        }
    });
    
    
    //Find first variable appearence in the code according to bipartite and returns the index
    function findFirstVarAppearance(){
        var indexFound = 0;
        var firstFound = false;
        var comment=false;
        var firstVar;
        var varName = $(".lineNumberLabel:eq(0)").text(); //first line number
        var total = global_bpData.length;
        var numOfVars = $(".varNameLabel").length;
        
        for (i=0;i<total;i++){
            if (varName == global_bpData[i][1] && !firstFound){
                firstFound=true;
                
                // alert(global_bpData[i][0]);

                var varNameBipartite = global_bpData[i][0];
                $(".mainrect").each (function(indexIn){
                    if(numOfVars>indexIn){
                        if($(".varNameLabel:eq("+indexIn+")").text() == varNameBipartite){
                            firstVar = $(".varNameLabel:eq("+indexIn+")").text();
                            return false;
                        }
                    }
                });
                break;
            }
        }
        
        firstFound=false;
        
        $("pre code table td:last-child").each (function(index){

            var lineText = $(this).text();
            //Remove meta-characters from line
            var lineTextMetas = replaceMetas(lineText);
            
            if(firstFound==false){
                //Check if there is a comment starting
                if (comment == false && lineText.search("\\/\\*")>-1){
                    comment=true;} 

                if(comment == true 
                   && (lineText.search("\\/\\*") > lineTextMetas.search(firstVar + " ") || lineText.search("\\/\\*") > lineTextMetas.search(" " + firstVar + " "))
                   && (lineTextMetas.search(firstVar + " ")>-1 || lineTextMetas.search(" " + firstVar + " ")>-1)){
                    firstFound=true;
                    indexFound = index;
                    return indexFound;
                }

                if ((lineTextMetas.search(firstVar + " ")>-1 || lineTextMetas.search(" " + firstVar + " ")>-1)
                   && comment == false && lineText.search(firstVar + ".h")<0){
                    firstFound=true;
                    indexFound = index;
                    return indexFound;
                }

                //Check if there is a comment ending
                if(lineText.search("\\*\\/")>-1){
                    comment=false;
                }
            }
        });
            
        return indexFound;
    }
    
    //Clone code to context view
    function loadContextView(){
        $("pre code table tbody").each (function(index){
            $(this).clone().insertAfter($(".code2 #contextView tbody")) 
        });
    }
    
    //Add the line numbers to the code according to the first found.
    function addLineNumbers(){
        var firstNum = parseInt($(".lineNumberLabel:eq(0)").text());
        var numOfVars = $(".varNameLabel").length;
        
        var initialCounter = firstNum - findFirstVarAppearance();
        
        $("pre code table td:first-child").each (function(index){
            $(this).text(index+initialCounter);//initialCounter or 1
            $(this).attr("title","line-"+(index+initialCounter));
            $(this).attr("class","line-"+(index+initialCounter));
            $("pre code table td:last-child:eq("+index+")").attr("title","codeLine-"+(index+initialCounter));
            $("pre code table td:last-child:eq("+index+")").attr("class","codeLine-"+(index+initialCounter));
        });
        loadContextView();
    }

    //SCROLL MASTER AND SLAVE TOGETHER
    $(document).ready(function(){
  
      var master = "source"; // this is id div
      var slave = "sourceMini"; // this is other id div
      var master_tmp;
      var slave_tmp;
      var timer;

      var sync = function ()
      {
        if($(this).attr('id') == slave)
        {
          master_tmp = master;
          slave_tmp = slave;
          master = slave;
          slave = master_tmp;
        }

        $("#" + slave).unbind("scroll");

        var percentage = this.scrollTop / (this.scrollHeight - this.offsetHeight);

        var x = percentage * ($("#" + slave).get(0).scrollHeight - $("#" + slave).get(0).offsetHeight);

        $("#" + slave).scrollTop(x);

        if(typeof(timer) !== 'undefind')
          clearTimeout(timer);

        timer = setTimeout(function(){ $("#" + slave).scroll(sync) }, 200)
      }

      $('#' + master + ', #' + slave).scroll(sync);

        
      //Scroll to element when slave is clicked
//      $("#contextView").on("click", "td:last-child", function(){
//        var clicked = $(this);
//        $("#contextView td:last-child").each (function(index){
//            var current = $(this);
//            if(current.is(clicked)){
//                var row = $("pre code table td:last-child:eq("+index+")");
//                var w = $("#" + master);
//                $('pre, code').animate({
//                    scrollTop: row.offset().top
//                }, 1000);   
//            }
//        });
//       });
    });
</script>