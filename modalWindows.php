<!-- Modal for adding new project -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-lg vertical-align-center">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Connect to your GitHub!</h4>
            </div>
            

        <form action="main.php" method="post" enctype="multipart/form-data">
            
            <div class="modal-body">
                <div class="container col-sm-12">
                    <div class="row rowSteps">
                        <div class="clearfix"></div>
                        <!-- <h2 class="section-heading-center fontCursive">How to connect</h2> -->


                        <div class="col-lg-3 col-sm-3 col-xs-6 miniDiv">
                            <div class="clearfix"></div>

                            <div class="col-sm-12 headerImg">
                                <img class="img-responsive" src="img/ico/cloud.png" alt="">
                            </div>

                            <h4 class="miniTitle text-center">Step 1</h4>
                            <p class="miniLead">Copy the '.git' URL of your project and paste it in the input box down below.</p>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-6 miniDiv">
                            <div class="clearfix"></div>

                            <div class="col-sm-12 headerImg">
                                <img class="img-responsive" src="img/ico/earth.png" alt="">
                            </div>

                            <h4 class="miniTitle text-center">Step 2</h4>
                            <p class="miniLead">Wait until our system maps your code and builds the data visualization for you.</p>
                        </div><div class="col-lg-3 col-sm-3 col-xs-6 miniDiv">
                            <div class="clearfix"></div>

                            <div class="col-sm-12 headerImg">
                                <img class="img-responsive" src="img/ico/pie.png" alt="">
                            </div>

                            <h4 class="miniTitle text-center">Step 3</h4>
                            <p class="miniLead">Select your new project on the dashboard area by clicking 'Slice Now'.</p>
                        </div><div class="col-lg-3 col-sm-3 col-xs-6 miniDiv">
                            <div class="clearfix"></div>

                            <div class="col-sm-12 headerImg">
                                <img class="img-responsive" src="img/ico/award.png" alt="">
                            </div>

                            <h4 class="miniTitle text-center">Step 4</h4>
                            <p class="miniLead"><b>That's it!</b> Enjoy using the visualizations to go through your project and check every detail of it!</p>
                        </div>


                    </div>


                </div>
                <!-- /.container -->
                
                    <div class="input-group margin-bottom-sm col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="input-group margin-bottom-sm gitHubURL">
                              <span class="input-group-addon"><i class="fa fa-github fa-fw"></i></span>
                              <input class="form-control loadingBar" type="text" placeholder="Enter Project's .git or .tar.gz URL" name="giturl" id="giturl">

                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <body>    OR</body>
                    </div>
                    <div class="modal-body">
                        <input type="file" name="fileToUpload" id="fileToUpload">
                    </div>
                     <div class="modal-body">
                        <button type="submit" class="btn btn-primary" value="Upload .tar.gz" name="submit">
                        <i class="fa fa-github"></i> Upload Program .tar.gz </button>
                     </div>
<!--
	        	<button type="submit" class="btn btn-primary" id="isupload" name="isUpload" value="true"><i class="fa fa-github"></i> Upload Program .tar.gz </button>
-->                    
	   <!-- </div> -->    
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-github"></i> Connect </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>

            </div>

        </form>
          </div>
        </div>
    </div>
  </div>
<script>
                            
    function codeSyntaxTheme(codeTheme){
        $("#codeThemeImg").attr("src","img/code-themes/"+codeTheme+".png");
        $("#codeThemeImg").attr("alt",codeTheme);
        $("#codeThemeImg").attr("title",codeTheme);
    }

</script>

<!-- Modal for preferences -->
<div class="modal fade" id="preferences" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-md vertical-align-center">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><i class="fa fa-edit"></i> Preferences</h4>
            </div>

            <div class="modal-body">
                <div class="container col-sm-12">
                    <div class="row rowSteps">
                        <div class="clearfix"></div>
                        <h2 class="section-heading-center fontCursive">Syntax Highlight</h2>


                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="clearfix"></div>
                            
                            <div class="syntaxImg">
                                <p><img class="img-responsive" src="img/code-themes/zenburn.png" alt="zenburn" title="zenburn" id="codeThemeImg"></p>
                            </div>
                            
                        </div>
                    </div>


                </div>
                <!-- /.container -->
                <form action="updatePreferences.php" method="post">

                    <div class="input-group margin-bottom-sm selectCodeTheme">
                        <div class="col-lg-12">
                            <p>
                            <select name="codeTheme" onchange="codeSyntaxTheme(this.value)">
                                <option value="zenburn">Zenburn</option>
                                <option value="default">Default</option>
                                <option value="vs">Vs</option>
                                <option value="github">Github</option>
                                <option value="monokai-sublime">Monokai-sublime</option>
                                <option value="agate">Agate</option>
                                <option value="androidstudio">Android Studio</option>
                                <option value="color-brewer">Color Brewer</option>
                            </select>
                            </p>
                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>

            </div>

                </form>
          </div>
        </div>
    </div>
  </div>

<!-- Modal for the tree map -->
<div class="modal fade" id="treeMap" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-lg vertical-align-center">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><i class="fa fa-map"></i> Tree Map</h4>
            </div>

            <div class="modal-body">
                <div class="container col-sm-12">
                    <div class="row">
                        <div class="clearfix"></div>

                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="clearfix"></div>
                            
                            <div class="col-sm-12" id="showChart">
                              <div id="chartTreemap"></div>
                            </div><!--/col-12-->
                            
                        </div>
                    </div>


                </div>
                <!-- /.container -->
                
                
                
                <div class="row rowVisualizations" id="displayChart">
                  <div class="col-lg-12">
                        <div class="row">
                            <div id="legend"></div>

                        </div>
                  </div><!--/col-12-->
                </div><!--/row-->
                
                <div class="input-group margin-bottom-sm selectCodeTheme">
                    <div class="col-lg-12">
                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <a class="btn btn-warning" href="navigation.php"><i class="fa fa-map"></i> Full Map</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>

            </div>

          </div>
        </div>
    </div>
  </div>


<!-- Modal Loading -->
<div class="modal fade" id="loading" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-xs vertical-align-center">
          <div class="modal-content text-center">
              <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><br>
              <span>Loading</span><br>
          </div>
        </div>
    </div>
  </div>
