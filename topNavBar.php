<!--TOP MENU-->    
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand sizeCursive" href="index.php"><img src="img/logo.png" class="logoMenu"> vizSlice</a>
          
          <button type="button" class="navbar-toggle pull-right hidden-md hidden-lg" data-toggle="collapse" data-target=".navbarRightMenu"> 
              <span class="sr-only ">Toggle navigation</span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
          </button>
        </div>
          
<!--        <ul class="nav navbar-nav">-->
<!--          <li><a href="#"><i class="fa fa-coffee"></i> Donate</a></li>-->
<!--        </ul>-->
          
        <ul class="nav navbar-nav navbar-right collapse navbar-collapse navbarRightMenu">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-github"></i> Projects<span class="caret"></span></a>
                
                <ul class="dropdown-menu projectsDropdown">
                    <!-- <li><a href="filesSession.php?filename=cvs-1.11.17"><i class="fa fa-cloud-download"></i> cvs-1.11.17</a></li> -->
                    <?php
                    // Dynamically generating the li tags
                        if (isset($_COOKIE['filesList'])){
                            if (sizeof($_COOKIE['filesList'])>0) {
                                $filesList = unserialize($_COOKIE['filesList']);
                                foreach ($filesList as $key => $value) {
                                   echo "<li>";
                                   echo "<a href='filesSession.php?filename=" . $value . "'>";
                                   echo "<i class='fa fa-cloud-download'></i> ";
                                   echo $value;
                                   echo "</a></li>";
                                }
                            }
                            else{
                                echo "<li><a href='#'>No projects yet.</a></li>";
                            }
                            
                        }
                        else{
                            echo "<li><a href='#'>No projects yet.</a></li>";
                        }
                    ?>

                    <li role="separator" class="divider"></li>
                    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add New Project</a></li>
                </ul>
          </li>
            
<!--          <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
<!--          <li><a href="#"> Login</a></li>-->
            
            
<!--WHEN LOGGED IN-->
<!--
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div class="miniProfilePicture">
                <img src="img/profilePic.jpg">
            </div>
          </a>
            <ul id="login-dp" class="dropdown-menu">
                <li>
                     <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                     <a href="profile.php" class="btn btn-primary btn-block">
                                         My profile
                                     </a>
                                     <a href="#" class="btn btn-danger btn-block">
                                         Log Out
                                     </a>
                                </div>
                            </div>
                     </div>
                </li>
            </ul>
        </li>
-->
            
<!--NOT LOGGED IN-->
<!--
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Sign Up <span class="caret"></span></a>
                <ul id="login-dp" class="dropdown-menu">
                    <li>
                         <div class="row">
                                <div class="col-md-12">
                                    Sign up via
                                    <div class="social-buttons">
                                        <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                        <a href="#" class="btn btn-success"><i class="fa fa-github"></i> GitHub</a>
                                    </div>
                                    or
                                     <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                            <div class="form-group">
                                                 <label class="sr-only" for="name">Name</label>
                                                 <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                                            </div>
                                            <div class="form-group">
                                                 <label class="sr-only" for="emailSignUp">Email address</label>
                                                 <input type="email" class="form-control" id="emailSignUp" placeholder="Email address" required name="emailSignUp">
                                            </div>
                                            <div class="form-group">
                                                 <label class="sr-only" for="pw1">Password</label>
                                                 <input type="password" class="form-control" id="pw1" placeholder="Password" required name="pw1">
                                            </div>
                                            <div class="form-group">
                                                 <label class="sr-only" for="pw2">Confirm Password</label>
                                                 <input type="password" class="form-control" id="pw2" placeholder="Confirm Password" required name="pw2">
                                            </div>
                                            <div class="form-group">
                                                 <button type="submit" class="btn btn-primary btn-block">Sign up</button>
                                            </div>
                                     </form>
                                </div>
                         </div>
                    </li>
                </ul>
            </li>
-->
<!--
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-log-in"></span> Login <span class="caret"></span></a>
                <ul id="login-dp" class="dropdown-menu">
                    <li>
                         <div class="row">
                                <div class="col-md-12">
                                    Login via
                                    <div class="social-buttons">
                                        <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                        <a href="#" class="btn btn-success"><i class="fa fa-github"></i> GitHub</a>
                                    </div>
                                    or
                                     <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                            <div class="form-group">
                                                 <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                 <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                            </div>
                                            <div class="form-group">
                                                 <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                 <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                 <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                                            </div>
                                            <div class="form-group">
                                                 <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                            </div>
                                            <div class="checkbox">
                                                 <label>
                                                 <input type="checkbox"> keep me logged-in
                                                 </label>
                                            </div>
                                     </form>
                                </div>
                         </div>
                    </li>
                </ul>
            </li>
-->

        </ul>
      </div>
    </nav>