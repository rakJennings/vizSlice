
# majority of file taken from  http://www.quesucede.com/page/show/id/python-3-tree-implementation
# modified for use in parser


class Node:
    def __init__(self, identifier):
        self.__identifier = identifier
        self.__children = []
        self.__data = {}

    @property
    def identifier(self):
        return self.__identifier

    @property
    def children(self):
        return self.__children

    def add_child(self, identifier):
        self.__children.append(identifier)

    def add_data(self, prop, value):
        self.__data[prop] = value

    def get_data(self, prop):
        return self.__data[prop]

    @property
    def props(self):
        return list(self.__data.keys())


(_ROOT, _DEPTH, _BREADTH) = range(3)


class Tree:

    def __init__(self):
        self.__root = None
        self.__nodes = {}

    @property
    def nodes(self):
        return self.__nodes

    def add_node(self, identifier, parent=None):
        node = Node(identifier)
        self[identifier] = node
        if parent is not None:
            self[parent].add_child(identifier)
        if self.__root is None:
            self.__root = identifier
        return node

    def display(self, identifier, depth=_ROOT):
        children = self[identifier].children
        if depth == _ROOT:
            print("{0}".format(identifier))
        else:
            print("\t"*depth, "{0}".format(identifier))

        depth += 1
        for child in children:
            self.display(child, depth)  # recursive call

    def traverse(self, identifier, mode=_DEPTH):
        # Python generator. Loosly based on an algorithm from
        # 'Essential LISP' by John R. Anderson, Albert T. Corbett,
        # and Brian J. Reiser, page 239-241
        yield identifier
        queue = self[identifier].children
        while queue:
            yield queue[0]
            expansion = self[queue[0]].children
            if mode == _DEPTH:
                queue = expansion + queue[1:]  # depth-first
            elif mode == _BREADTH:
                queue = queue[1:] + expansion  # width-first

    def __getitem__(self, key):
        return self.__nodes[key]

    def __setitem__(self, key, item):
        self.__nodes[key] = item

    def __contains__(self, key):
        return key in self.nodes

    def add_node_path(self, path, deliminator='/'):
        for i in range(1, len(path)+1):
            node = '/'.join(path[0:i])
            if node not in self:
                if i == 1:
                    p = None
                else:
                    p = deliminator.join(path[0:i-1])
                self.add_node(node, parent=p)
    @property
    def get_root(self):
        return self.__root
