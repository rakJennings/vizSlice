import statistics
import sys
sys.path.append("./classes/")
from tree import Tree

# defined as data
defined = ['def', 'use', 'dvars', 'pointers', 'cfuncs']
# defined as calculated data
calc_defined = ['sl avg', 'sl size']


def get_sample(t):
    sample = []
    for node in t.traverse(t.get_root):
        if len(t[node].children) > 0:
            sample += [t[node].get_data(calc_defined[0])]
    return sample


# performs statistics calculation for tree object
def get_stats(t):
    stats = [0, 0, 0, 0, 0]
    sample = get_sample(t)
    sample.sort()
    stats[0] = min(sample)
    stats[4] = max(sample)
    stats[2] = statistics.median_low(sample)
    med = sample.index(stats[2])
    stats[1] = statistics.median_low(sample[0:med])
    stats[3] = statistics.median_high(sample[med+1:])
    return stats


# writes data to file.
def viz_to_file(t, f_name):
    #print(t[t.get_root].children)
    avg_slice_calc(t, [t.get_root])
    stats = get_stats(t)
    with open(f_name[:-3] + "json", 'w') as f:
        f.write("[" + viz_to_json(t, [t.get_root])[:-1] + "," + str(stats) + "]")


# This is a convenience function.  Use this function to convert a file to a json data file.
def convert(f):
    viz_to_file(parse(f), f)


# converts tree data to json data format. Will need to be updated depending on what data we want to have.
# goes through the tree node by node and uses recursion.
def viz_to_json(t, children):
    json = ''
    for node in children:
        node_name = node.split('/')[-1]
        if any(prop in defined for prop in t[node].props):
            json = json + '{"name":"' + node_name + '"'

            item1 = str(t[node].get_data(defined[0]))
            item2 = str(t[node].get_data(defined[1]))

            sl = []  # init sl
            if len(item1) != 0 and len(item2) != 0:
                sl = (item1 + ',' + item2).split(',')
            elif len(item1) == 0 and len(item2) != 0:
                sl = item2.split(',')
            elif len(item1) != 0 and len(item2) == 0:
                sl = item1.split()

            json += ',"value":' + str(len(sl))
            json += ',"SLICING_LINES":['
            tmp = ','.join(str(x) for x in sl)
            json += tmp
            json += ']},'
        else:
            json += '{"name":"' + node_name + '","avg":' + str(t[node].get_data(calc_defined[0]))
            json += ',"children":['
            json += viz_to_json(t, t[node].children)
            json = json[:-1] + ']},'
    return json


# Calculates avg slicing size for each level
def avg_slice_calc(t, children):
    avg = 0
    for node in children:
        if len(t[node].children) == 0:
            avg += t[node].get_data(calc_defined[1])
        else:
            node_avg = avg_slice_calc(t, t[node].children)
            t[node].add_data(calc_defined[0], node_avg)
            avg += node_avg
    return avg/len(children)


# def get property data
def get_props(data):
    prop_dict = dict()
    for prop in defined:
        s = data.find(prop) + len(prop)
        lc = rc = 0
        i = j = s + data[s:].find('{') + 1
        lc += 1
        while lc != rc:
            if data[j] is '{':
                lc += 1
            if data[j] is '}':
                rc += 1
            j += 1
        prop_dict[prop] = data[i:j-1]
    return prop_dict


# the meat of this program.  Converts the srcSlice data into a tree object.
def parse(fileName):
    viz_tree = Tree()
    with open(fileName, 'r') as f:
        for line in f:
            line = line.strip()
            left_count = line.count('{')
            right_count = line.count('}')
            # This loop gets an entire data line if it is broken up between multiple lines using the \n char
            # assumes that there is another line, IT WILL BREAK IF THERE ISN'T OR IF THE BRACKETS ARE SCREWED UP
            while left_count != right_count or line[-1:] == ',':
                line += f.readline().strip()
                left_count = line.count('{')
                right_count = line.count('}')

            # extract path then rejoin rest of data to single string
            data = line.split(',')
            path = '/'.join(data[:3]).split('/')  # combines them into a file path then splits the whole thing
            data = ','.join(data[3:])

            # if there is data
            if len(data) > 0:
                # get properties of data line
                prop_dict = get_props(data)
                # adds properties to tree
                for key in prop_dict:
                    viz_tree.add_node_path(path)
                    viz_tree['/'.join(path)].add_data(key, prop_dict[key])
                lines = prop_dict[defined[0]].split(',') + prop_dict[defined[1]].split(',')
                lines = [x for x in lines if x != '' and x.isdigit()]
                viz_tree['/'.join(path)].add_data(calc_defined[1], len(lines))
    return viz_tree

#Calls convert on first command line argument if called from command line
#use: python3.4 slicemlParser.py path/to/slice/xml/files
#Make sure to run from parser directory
if __name__ == "__main__":
    convert(sys.argv[1])
