<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php include "header.php"; ?>

<body>
    
    <?php include "topNavBar.php"; ?>
    
    <div id="wrapper" class="toggled">  <!--class="toggled"-->
        
        <?php include "leftBar.php"; ?>
        
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="fontCursive"><i class="fa fa-dashboard"></i> Dashboard</h1>
                        
                        <hr>        
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-success"><span class="label label-success pull-right">18</span> Projects </h4>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-primary"><span class="label label-primary pull-right">20%</span> Projects in use </h4>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-warning"><span class="label label-warning pull-right">342 MB</span> Downloaded </h4>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-danger"><span class="label label-danger pull-right">123</span> Accesses </h4>
                                </div>
                              </div>
                            </div><!--/row-->
                            
                          </div><!--/col-12-->
                        </div><!--/row-->
                        
                        <hr>
                        <h2 class="lastProjects"><i class="fa fa-laptop"></i> Latest Projects <span class="label label-info">Total: <?php if (isset($filesList)){echo sizeof($filesList);} else {echo 0;}?></span></h2>
                        <div class="row">
                            <?php
                              // Dynamically generate divs with files for slices
                              if (isset($_COOKIE['filesList'])){
                                if (sizeof($_COOKIE['filesList'])>0) {
                                  $numProj = 1;
                                  foreach ($filesList as $cont => $value) {
                                    echo '<div class="col-md-3">';
                                    echo '<div class="well">';
                                    echo '<h4 class="text-success"><span class="label label-success pull-right">' . $numProj;
                                    echo '</span><i class="fa fa-file-code-o"></i> ' . $value . '</h4><hr>';
                                    echo '<h5>Lorem ipsum dolor sit amet, consectetur' .
                                        ' adipiscing elit. Fusce semper tellus lacinia.</h5>';

                                    echo '<div class="row btnMargin">';

                                    echo '<div class="col-lg-6 col-md-12 col-sm-12 text-center">';
                                    echo '<a class="btn btn-success" href="filesSession.php?filename=' . $value . '">';
                                    echo '<span>Slice Now</span>';
                                    echo '<i class="fa fa-chevron-right" title="Slice Now" aria-hidden="true"></i>';
                                    echo '</a></div>';

                                    echo '<div class="col-lg-6 col-md-12 col-sm-12 text-center">';
                                    echo '<div class="row">';

                                    echo '<a class="btn btn-info" href="#">';
                                    echo '<i class="fa fa-info" title="Information" aria-hidden="true"></i>';
                                    echo '<span class="sr-only">Information</span></a> ';

                                    echo '<a class="btn btn-warning" href="#">';
                                    echo '<i class="fa fa-cog" title="Edit" aria-hidden="true"></i>';
                                    echo '<span class="sr-only">Edit</span></a> ';

                                    echo '<a class="btn btn-danger" href="deleteProj.php?name='.$value.'">';
                                    echo '<i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i>';
                                    echo '<span class="sr-only">Delete</span></a>';
                                    
                                    echo '</div></div></div>';

                                    echo '<hr>';

                                    echo '<div class="btn-group btnWidth">';
                                    echo '<button type="button" class="btn btn-primary dropdown-toggle btnWidth" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                    echo '<i class="fa fa-download"></i> Downloads <span class="caret"></span></button>';
                                    echo '<ul class="dropdown-menu btnWidth">';
                                    echo '<li><a href="downloadGeneratedFile.php?name='.$value.'&type=1"><i class="fa fa-download"></i></button> srcMl</a></li>';
                                    echo '<li><a href="downloadGeneratedFile.php?name='.$value.'&type=2"><i class="fa fa-download"></i></button> srcMl w/Position</a></li>';
                                    echo '<li><a href="downloadGeneratedFile.php?name='.$value.'&type=3"><i class="fa fa-download"></i></button> srcSlice</a></li>';
                                    echo '<li><a href="downloadGeneratedFile.php?name='.$value.'&type=4"><i class="fa fa-download"></i></button> Json File</a></li>';

                                    echo '</ul></div>';

                                    echo '</div></div>';

                                    // echo "<a href='filesSession.php?filename=" . $value . "'>";
                                    // echo "<i class='fa fa-cloud-download'></i> ";
                                    // echo $value;
                                    // echo "</a></li>";
                                    $numProj++;
                                  }
                                }
                                else{
                                  echo "<div style='text-align:center'><button href='#' data-toggle='modal' data-target='#myModal' class='btn btn-success btnNewProj'><i class='fa fa-plus'></i> Add New Project</button></div>";
                                }
                              }
                              else{
                                echo "<div style='text-align:center'><button href='#' data-toggle='modal' data-target='#myModal' class='btn btn-success btnNewProj'><i class='fa fa-plus'></i> Add New Project</button></div>";
                              }
                              
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    <?php 
        include "modalWindows.php"; 
        include "includeScripts.php";
    ?>
    

</body>
</html>
