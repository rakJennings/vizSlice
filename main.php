<?php
$targetURL = $_POST["giturl"];
$cookie_name = "user";
if(!isset($_COOKIE[$cookie_name])){
	setcookie($cookie_name, $targetURL, time() + (86400 * 90),"/");
}
else {
	$value = $_COOKIE[$cookie_name];
	$urls = explode("~", $value);
	if(count($urls) < 30) {
		$doAdd = True;
		for($j = 0; $j < count($urls); $j++) {
			if($urls[$j] == $targetURL) {
				$doAdd = False;
			}
		}
		if($doAdd == True) {
			$newValue = $value."~".$targetURL;
			setcookie($cookie_name, $newValue, time() + (86400 * 90),"/");
		}
		else {
			setcookie($cookie_name, $value, time() + (86400 * 90),"/");
		}
	}
}
?>

<html>
<body>

	<?php
	//ini_set('display_errors', 'On');
	//error_reporting(E_ALL);

	use GitWrapper\GitWrapper;

	//use correct http/https
	$currentURL = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}/{$_SERVER['REQUEST_URI']}";
	$currentPath = trim(shell_exec('pwd'));
	$targetURL = $_POST["giturl"];
//	$isUpload = $_POST["isUpload"];

	error_log("\nBegin error log entry: ".$targetURL."\n".date("h:i:sa").' '.date("Y-m-d")."\n", 3, $currentPath.'/php.log');
////	error_log("\nisUpload: ".$isUpload."\n", 3, $currentPath.'/php.log');

	$repoDir = '';
	$usernameDir = '';
	$pathname = $currentPath.'/repos/';
	$filemode = 0777;

	//TODO: stop using 777 to make permissions work
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathname));
        foreach($iterator as $item) {
        	//@ to supress errors
        	@chmod($item, $filemode);
        }

	$pieces = explode("/", $targetURL);

	$gitMatch = preg_match("#.*/.+/.+\.git$#", $targetURL);
	$tarMatch = preg_match("#.*\.tar.*#", $targetURL);

	if($gitMatch) { //good url
		error_log("git Url\n", 3, $currentPath.'/php.log');

		//check and remove leading https
		$pos = strpos($pieces[0], ':');
		if ($pos !== false) {
			$newURL = $pieces[2];
			for($i = 3; $i < count($pieces); $i++) {
				$newURL = $newURL.'/'.$pieces[$i];
			}
			$targetURL = $newURL;
		}

		$usernameDir = $pieces[count($pieces)-2];
		$findRepo = $pieces[count($pieces)-1];
		$repoGit = explode(".",$findRepo);
		$repoDir = $repoGit[0];

		$checkPath = $currentPath.'/xml_files/'.$repoDir.'.xml';
		if (!file_exists($checkPath)){ //begin check if alread sliced

			// Initialize the library. If the path to the Git binary is not passed as
			// the first argument when instantiating GitWrapper, it is auto-discovered.
			require_once('vendor/autoload.php');
			$wrapper = new GitWrapper();

			// Optionally specify a private key other than one of the defaults.
			//$wrapper->setPrivateKey('/path/to/private/key');

			// Clone a repo into `/path/to/working/copy`, get a working copy object.
			$cloneString = 'git://' . $targetURL;
			error_log("clone Url: ".$cloneString."\n",3, $currentPath.'/php.log');
			$repoPath = $pathname.$repoDir;
			if (!file_exists($repoPath)) {
				mkdir($repoPath, 0777);
				error_log("repoPath: ".$repoPath."\n",3, $currentPath.'/php.log');

				$git = $wrapper->clone($cloneString, $repoPath);

				//Make repository deletable via commandline
				$newPath = $pathname.$repoDir;
				$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($newPath));
				foreach($iterator as $item) {
					//@ to supress errors
			    		@chmod($item, $filemode);
				}
			}

			//change to repos directory and run script for srcml and srcslice
			chdir($pathname);
			$script = './srcBash '.$repoDir.' '.$repoDir.' 2>&1';
			$output = shell_exec($script);

		        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathname));
		        foreach($iterator as $item) {
		        	//@ to supress errors
		                @chmod($item, $filemode);
		        }

			//MOVE README
		        @rename($usernameDir.'/'.$repoDir.'/README.md', '../readme_files/'.$repoDir.'.readme');
			
			//remove downloaded repo
			shell_exec('rm -r '.$repoDir.'/');
		}//end check if already sliced if
	}//end of check github input if
	elseif($tarMatch | isset($_POST["submit"])) {
		if(isset($_POST["submit"])) {
			$target_dir = $currentPath."/repos/";
			$filename = basename($_FILES["fileToUpload"]["name"]);
			$target_file = $target_dir.$filename;
			error_log("target file: ".$target_file."\n", 3, $currentPath.'/php.log');
			$uploadOk = 1;
			$fileType = pathinfo($target_file,PATHINFO_EXTENSION);	
			if ($fileType != 'gz') {
				$uploadOk = 0;
				error_log("Bad File Type: ".$fileType."\n", 3, $currentPath.'/php.log');
			}
			if ($uploadOk == 1) {
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					error_log("File has been uploaded\n", 3, $currentPath.'/php.log');
				}
				else {
					error_log("Error uploading the file\n", 3, $currentPath.'/php.log');
				}
			}
			else {
				// TODO: Notify user of bad file upload
				error_log("Bad File Type: ".$fileType."\n", 3, $currentPath.'/php.log');
			}
			$pos = strpos($filename, ".tar");
			$repoDir = substr($filename, 0, $pos);
			chdir($pathname);
			$renameTar = 'mkdir '.$repoDir.' && tar -xf '.$filename.' -C '.$repoDir.' --strip-components 1';
		}// end of Tar upload
		else {
			error_log("Tar Url\n", 3, $currentPath.'/php.log');
			$pieces = explode("/", $targetURL);
			$filename = $pieces[count($pieces)-1];
			$pos = strpos($filename, ".tar");
			$repoDir = substr($filename, 0, $pos);
			
			//check for master from github tar
			$oldRepoDir = $repoDir;
			$fromGithub = strpos($targetURL, 'github');
			if($repoDir == 'master' && ($fromGithub != false)) {
				$repoDir = $pieces[count($pieces)-3];
			}
			error_log("repoDir: ".$repoDir."\n", 3, $currentPath.'/php.log');

			chdir($pathname);

			$getFile = file_get_contents($targetURL);
			file_put_contents($filename, $getFile);
			@chmod($filename, $filemode);
		
			$renameTar = 'mkdir '.$repoDir.' && tar -xf '.$filename.' -C '.$repoDir.' --strip-components 1';
		}// end of tar URL
		//$renameScript = 'tar -xf '.$filename.' && mv '.$oldRepoDir.'/ '.$repoDir.'/';
		$renameOutput = shell_exec($renameTar);
		$srcBashScript = './srcBash '.$repoDir.'/ '.$repoDir.' 2>&1';
		$scrBashOutput = shell_exec($srcBashScript);
		
		shell_exec('rm '.$filename);
		shell_exec('rm -r '.$repoDir.'/');
	}
	else {
		 //TODO: Inform user of bad URL
		error_log("Bad Url\n", 3, $currentPath.'/php.log');
		header('Location: index.php');
		exit;
	}


	//Parse
	shell_exec('python3 slicemlParser.py '.$repoDir.'-srcSlice.xml');
	chdir('../');
	 
	//move files to correct locations with simple names
	@chmod('repos/'.$repoDir.'-srcMl.xml', $filemode);
	@chmod('repos/'.$repoDir.'-srcMlNoPos.xml', $filemode);
	@chmod('repos/'.$repoDir.'-srcSlice.json', $filemode);
	@chmod('repos/'.$repoDir.'-srcSlice.xml', $filemode);
	rename('repos/'.$repoDir.'-srcMl.xml', 'xml_files/'.$repoDir.'WithPos.xml');
	rename('repos/'.$repoDir.'-srcMlNoPos.xml', 'xml_files/'.$repoDir.'.xml');
	rename('repos/'.$repoDir.'-srcSlice.json', 'json_files/'.$repoDir.'.json');
	rename('repos/'.$repoDir.'-srcSlice.xml', 'slice_files/'.$repoDir.'.slice.xml');
	

	//return to home page
	header('Location: index.php');
	die();

	//echo "cookie: ".$_COOKIE[$cookie_name];
	?>
</body>
</html>
