<?php
$targetURL = $_GET["giturl"];
$cookie_name = "user";
if(!isset($_COOKIE[$cookie_name])){
	setcookie($cookie_name, $targetURL, time() + (86400 * 90),"/");
}
else {
	$value = $_COOKIE[$cookie_name];
	$urls = explode("~", $value);
	if(count($urls) < 30) {
		$doAdd = True;
		for($j = 0; $j < count($urls); $j++) {
			if($urls[$j] == $targetURL) {
				$doAdd = False;
			}
		}
		if($doAdd == True) {
			$newValue = $value."~".$targetURL;
			setcookie($cookie_name, $newValue, time() + (86400 * 90),"/");
		}
		else {
			setcookie($cookie_name, $value, time() + (86400 * 90),"/");
		}
	}
}
?>

<html>
<body>

	<?php
	use GitWrapper\GitWrapper;

	$currentPath = trim(shell_exec('pwd'));
	//$userPieces = explode("/", $user);
	//$user = $userPieces[2];

	$repoDir = '';
	$usernameDir = '';
	$pathname = $currentPath.'/repos/';
	$filemode = 0777;
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathname));
        foreach($iterator as $item) {
        	//@ should supress errors
        	@chmod($item, $filemode);
        }

	$targetURL = $_GET["giturl"];
	$pieces = explode("/", $targetURL);

	if(preg_match("#.*github.com/.+/.+\.git$#",$targetURL)) {
		//goodurl


		//check and remove leading https
		$pos = strpos($pieces[0], ':');
		if ($pos !== false) {
			$newURL = $pieces[2];
			for($i = 3; $i < count($pieces); $i++) {
				$newURL = $newURL.'/'.$pieces[$i];
			}
			$targetURL = $newURL;
		}
		$usernameDir = $pieces[count($pieces)-2];
		$findRepo = $pieces[count($pieces)-1];
		$repoGit = explode(".",$findRepo);
		$repoDir = $repoGit[0];

		$checkPath = $pathname.$repoDir.'-srcMl.xml';
		if (!file_exists($checkPath)){ //begin check if alread sliced

			// Initialize the library. If the path to the Git binary is not passed as
			// the first argument when instantiating GitWrapper, it is auto-discovered.
			require_once 'vendor/autoload.php';
			$wrapper = new GitWrapper();

			// Optionally specify a private key other than one of the defaults.
			//$wrapper->setPrivateKey('/path/to/private/key');

			// Clone a repo into `/path/to/working/copy`, get a working copy object.
			$cloneString = 'git://' . $targetURL;
			$repoPath = $pathname.$usernameDir.'/'.$repoDir;
			if (!file_exists($repoPath)) {
				mkdir($pathname.$usernameDir, 0777);
				$git = $wrapper->clone($cloneString, $repoPath);

				//Make repository deletable via commandline
				$newPath = $pathname.$usernameDir;
				$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($newPath));
				foreach($iterator as $item) {
					//@ should supress errors
			    		@chmod($item, $filemode);
				}
			}
			chdir($pathname);
			$script = $pathname.'srcBash '.$repoPath.' '.$repoDir.' 2>&1';
			$output = shell_exec($script);

		        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathname));
		        foreach($iterator as $item) {
		        	//@ should supress errors
		                @chmod($item, $filemode);
		        }

			shell_exec('php clear.php '.$usernameDir.' '.$repoDir.' > /dev/null 2>&1 &');
		}//end check if already sliced if
	}//end of check input if

	else {
		echo 'Bad URL';
	}

	//echo "cookie: ".$_COOKIE[$cookie_name];
	?>

	<form action = "downloadMl.php" method = "get">
		<input type = "submit" value = "Download srcML File">
		<input type = "hidden" name = "name" value = "<?php echo $repoDir;?>" />
	</form>

	<form action = "downloadSlice.php" method = "get">
		<input type = "submit" value = "Download srcSlice File">
		<input type = "hidden" name = "name" value = "<?php echo $repoDir;?>" />
	</form>

	<a href = "http://www.users.miamioh.edu/kaczkara/slicing/treemap/"> Visualize </a>

	<form action = "http://ceclnx01.cec.miamioh.edu/~pitstile/github/">
		<input type = "submit" value = "Back">
	</form>
</body>
</html>
