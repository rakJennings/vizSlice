<?php
$name = $_GET['name'];
$currentPath = trim(shell_exec('pwd'));
$file = $currentPath.'/repos/'.$name.'-srcMl.xml';

if(file_exists($file)) {
        header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
}

?>
