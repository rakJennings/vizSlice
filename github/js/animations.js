$(document).ready(function(){

    $('a[href^="#"]').on('click', function(event) {    
        var target = $(this).attr("href");

        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 800);
        }
    });
    
    $("#connect").on('click', function(){
        $("#connect span:eq(1)").remove();
        $("#connect .spin1").addClass("fa fa-circle-o-notch fa-counter-spin");
        
        //$(".nextSpin").addClass("fa-counter-spin");
    
        
        $("#connect").removeClass("fullButton");
        $("#connect").addClass("loading");
        
    });
    

});
