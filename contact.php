<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php include "header.php"; ?>

<body>
    <?php include "topNavBar.php"; ?>
    
    <div id="wrapper" class="toggled">  <!--class="toggled"-->
        
        <?php include "leftBar.php"; ?>
        
        
        
        <!-- Page Content -->
        <div id="page-content-wrapper" class="page-content-wrapper-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12 mapTitle">
                                <h1 class="fontCursive"><i class="glyphicon glyphicon-send"></i> Contact Us</h1>
                                <hr/>
                            </div>

                            <div class="col-lg-6">
                                <section class="content contentContact">
                                    <span class="input input--haruki input--bigger">
                                        <input class="input__field input__field--haruki" type="text" id="input-1"  />
                                        <label class="input__label input__label--haruki" for="input-1">
                                            <span class="input__label-content input__label-content--haruki">Name</span>
                                        </label>
                                    </span>
                                    <span class="input input--haruki">
                                        <input class="input__field input__field--haruki" type="email" id="input-3" />
                                        <label class="input__label input__label--haruki" for="input-3">
                                            <span class="input__label-content input__label-content--haruki">Email</span>
                                        </label>
                                    </span>
                                    <span class="input input--haruki">
                                        <input class="input__field input__field--haruki" type="text" id="input-4" />
                                        <label class="input__label input__label--haruki" for="input-4">
                                            <span class="input__label-content input__label-content--haruki">Phone</span>
                                        </label>
                                    </span>
                                    <span class="input2 input--haruki">
                                        <textarea class="input__field input__field--haruki textarea--paulovs" id="input-5"></textarea>
                                        <label class="input__label input__label--haruki input__label--paulovs" for="input-5">
                                            <span class="input__label-content input__label-content--haruki">Message</span>
                                        </label>
                                    </span>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <div class="btn-group"> 
                                                <button type="button" class="btn btn-lg btn-danger btn-sendMessage">
                                                    Send message
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </div><!-- /first half -->
                            
                            <div class="col-lg-6">                              
                                <div class="row text-center informationContact">
                                    <div class="col-sm-6 col-xs-12 first-box">
                                        <h1><span class="glyphicon glyphicon-earphone"></span></h1>
                                        <h3>Phone</h3>
                                        <p>+1 000 000 0000</p><br>
                                    </div>
                                    <div class="col-sm-6 col-xs-12 second-box">
                                        <h1><span class="glyphicon glyphicon-home"></span></h1>
                                        <h3>Location</h3>
                                        <p>501 E. High St. Oxford, OH 45056</p><br>
                                    </div>
                                    <div class="col-sm-6 col-xs-12 third-box">
                                        <h1><span class="glyphicon glyphicon-send"></span></h1>
                                        <h3>E-mail</h3>
                                        <p><a href="#">contact@vizSlice.com</a></p><br>
                                    </div>
                                    <div class="col-sm-6 col-xs-12 fourth-box">
                                        <h1><span class="glyphicon glyphicon-leaf"></span></h1>
                                        <h3>Web</h3>
                                        <p><a href="#">www.miamioh.edu</a></p><br>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-sm-12 col-lg-12">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d12313.387124564275!2d-84.72633185!3d39.506662299999995!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1466614614739" width="100%" height="250px" frameborder="0" style="border:0; margin-top:50px;" allowfullscreen></iframe>
                                    </div>
                                </div>
                                
                            </div>
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    <?php 
        include "modalWindows.php"; 
        include "includeScripts.php";?>
    

</body>
</html>
