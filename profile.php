<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php include "header.php"; ?>

<body>
    <?php include "topNavBar.php"; ?>
    
    <div id="wrapper" class="toggled">  <!--class="toggled"-->
        
        <?php include "leftBar.php"; ?>
        
        <div class="leftProfile">
            <div class="profilePicture">
                <img src="img/profilePic.jpg">
            </div>
            
            <h1 class="profileName fontCursive">
                Paulo de Souza
            </h1>
            
            <h3 class="profilePosition">
                Web Developer
            </h3>
            
            <ul class="list-group">
                <!--<li class="list-group-item"></li>-->
                <li class="list-group-item">
                    <span class="fa-stack">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <a href="http://virotedesouza.com/">virotedesouza.com</a>
                </li>
                <li class="list-group-item">
                    <span class="fa-stack">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <a href="mailto:paulosouzav@gmail.com">paulosouzav@gmail.com</a>
                </li>
                <li class="list-group-item">
                    <span class="fa-stack">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-github-alt fa-stack-1x fa-inverse"></i>
                    </span>
                    <a href="https://github.com/paulosouzav">github.com/paulosouzav</a>
                </li>
                <li class="list-group-item">
                    <span class="fa-stack">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                    </span>
                    <a href="https://www.facebook.com/paulo.vdsouza">fb.com/paulo.vdsouza</a>
                </li>
                <li class="list-group-item">
                    <span class="fa-stack">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                    <a href="https://twitter.com/pauloovs">twitter.com/pauloovs</a>
                </li>
            </ul>
            
        </div>
        
        <!-- Page Content -->
        <div class="rightProfile">
            <div id="page-content-wrapper2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="well">
                                    <h2 class="fontCursive text-danger">Who am I?</h2>
                                    <hr> 
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in condimentum enim. Morbi vel ipsum id neque tristique molestie. Etiam blandit tincidunt rhoncus. Etiam tincidunt velit sed elit malesuada, quis iaculis lorem porttitor. Suspendisse a ante erat. Ut ut vehicula velit. Mauris a lectus mi. Proin dictum lorem ut lorem mollis scelerisque. Mauris sed dui aliquam, ultrices quam id, pharetra lacus. Suspendisse finibus commodo elit, vitae dictum magna cursus id. Aenean dapibus non nulla id placerat. Pellentesque a libero vitae tortor mattis efficitur. Duis quis sapien dui. Donec eget nisi odio. Proin sagittis posuere lacus, ut faucibus quam posuere et. Cras pellentesque mattis massa in rutrum. Curabitur pretium neque sed malesuada aliquam. Nunc porttitor sit amet lorem sit amet varius.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in condimentum enim. Morbi vel ipsum id neque tristique molestie. Etiam blandit tincidunt rhoncus. Etiam tincidunt velit sed elit malesuada, quis iaculis lorem porttitor. Suspendisse a ante erat. Ut ut vehicula velit. Mauris a lectus mi. Proin dictum lorem ut lorem mollis scelerisque. Mauris sed dui aliquam, ultrices quam id, pharetra lacus. Suspendisse finibus commodo elit, vitae dictum magna cursus id. Aenean dapibus non nulla id placerat. Pellentesque a libero vitae tortor mattis efficitur. Duis quis sapien dui. Donec eget nisi odio. Proin sagittis posuere lacus, ut faucibus quam posuere et. Cras pellentesque mattis massa in rutrum. Curabitur pretium neque sed malesuada aliquam. Nunc porttitor sit amet lorem sit amet varius.</p>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-danger">
                                      <i class="fa fa-map-marker"></i>
                                      Address
                                    <h5>
                                        703 Avenida Perimetral Leste, Charqueadas, RS, Brazil
                                    </h5>  
                                  </h4>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-danger">
                                      <i class="fa fa-calendar"></i>
                                      Date of Birth
                                    <h5>
                                        12/31/1992
                                    </h5>  
                                  </h4>
                                </div>
                              </div>
                                <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-danger">
                                      <i class="fa fa-graduation-cap"></i>
                                      Education
                                    <h5>
                                        Computer Science Major
                                    </h5>  
                                  </h4>
                                </div>
                              </div>
                                <div class="col-md-3">
                                <div class="well">
                                  <h4 class="text-danger">
                                      <i class="fa fa-mobile"></i>
                                      Phone
                                    <h5>
                                        +55 51 3658 3720
                                    </h5>  
                                  </h4>
                                </div>
                              </div>
                            </div><!--/row-->
                            
                          </div><!--/col-12-->
                        </div><!--/row-->
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    <?php 
        include "modalWindows.php"; 
        include "includeScripts.php";?>
    

</body>
</html>
