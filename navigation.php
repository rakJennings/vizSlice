<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<?php include "header.php"; ?>

<body>
    <?php include "topNavBar.php"; ?>
    
    <div id="wrapper" class="toggled">  <!--class="toggled"-->
        
        <?php include "leftBar.php"; ?>
        
        <!-- Page Content -->
        <div id="page-content-wrapper" class="navigationPage">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="legend"></div>
                        <h1 class="fontTitle">
                            <span class="fa-stack">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-file-code-o fa-stack-1x fa-inverse"></i>
                            </span>
                            <span id="directory">
                                <?php echo $_SESSION['filename']; ?>
                            </span>
                        </h1>
                        <hr>        
                        <div class="row" >
                          <div class="col-sm-12" id="showChart">
                              <div><p id="chartTreemap"></p></div>
                          </div><!--/col-12-->
                        </div><!--/row-->
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    <?php 
        include "modalWindows.php"; 
        include "includeScripts.php";?>
    
    <script>
        $('#loading').modal('show');
    </script>
    
    <?php
        include "mapScripts.php";
    ?>

</body>
</html>
