
<script>

var margin = {top: 30, right: 0, bottom: 20, left: 0},
    
    <?php
       if (strcmp($currentFile,"navigation.php")==0){
         echo "width = document.getElementById('showChart').offsetWidth - margin.left - margin.right,";
         echo "height = document.getElementById('showChart').offsetHeight - margin.top - margin.bottom,";
       }
       else{
         echo "width = 800,";
         echo "height = 400 - margin.top - margin.bottom,";
       }
    ?>
    // width = 960,        
    // height = 500 - margin.top - margin.bottom,
    formatNumber = d3.format(",%"),
    colorDomain = [-1, 0, 1],
    colorRange = ["#373a93", 'white', "#936638"],
    transitioning;

// sets x and y scale to determine size of visible boxes
var x = d3.scale.linear()
    .domain([0, width])
    .range([0, width]);

var y = d3.scale.linear()
    .domain([0, height])
    .range([0, height]);

// adding a color scale
//var color= d3.scale.linear()
    //.domain([0,d3.min()])
    //.range(["green", "yellow", "red"]);
var color;
// introduce color scale here

var treemap = d3.layout.treemap()
    .children(function(d, depth) { return depth ? null : d._children; })
    .sort(function(a, b) { return a.value - b.value; })
    .ratio(height / width * 0.5 * (1 + Math.sqrt(5)))
    .round(false);

var svg = d3.select("#chartTreemap").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.bottom + margin.top)
    .style("margin-left", -margin.left + "px")
    .style("margin.right", -margin.right + "px")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .style("shape-rendering", "crispEdges");

var grandparent = svg.append("g")
    .attr("class", "grandparent");

grandparent.append("rect")
    .attr("y", -margin.top)
    .attr("width", width)
    .attr("height", margin.top);

grandparent.append("text")
    .attr("x", 6)
    .attr("y", 6 - margin.top)
    .attr("dy", ".75em");

function getSample(t){
  var sample = [];
  if(typeof t.children !== "undefined"){
    t.children.forEach(function(d){
      sample = sample.concat(getSample(d)).concat(t.avg);
    })
    return sample;
  } else if (typeof t._children !== "undefined") {
    t._children.forEach(function(d){
      sample = sample.concat(getSample(d)).concat(t.avg);
    })
    return sample;
  } else {
    return sample;
  }
}

function getStats(s){
  var stats = [0,0,0,0,0];
  stats[2] = s.reduce((prev,curr)=>prev+curr)/s.length;
  var sd = s.reduce((prev,curr)=> prev+Math.pow((curr-stats[2]),2));
  sd = sd/s.length;
  sd = Math.sqrt(sd);
  stats[1] = stats[2]-sd;
  stats[3] = stats[2]+sd;
  stats[4] = Math.max(...s);
  return stats;
}

function initialize(root) {
    root.x = root.y = 0;
    root.dx = width;
    root.dy = height;
    root.depth = 0;
  }

  // Aggregate the values for internal nodes. This is normally done by the
  // treemap layout, but not here because of our custom implementation.
  // We also take a snapshot of the original children (_children) to avoid
  // the children being overwritten when when layout is computed.
  function accumulate(d) {
    return (d._children = d.children)
      // recursion step, note that p and v are defined by reduce
        ? d.value = d.children.reduce(function(p, v) {return p + accumulate(v); }, 0)
        : d.value;
  }

  
  // Compute the treemap layout recursively such that each group of siblings
  // uses the same size (1×1) rather than the dimensions of the parent cell.
  // This optimizes the layout for the current zoom state. Note that a wrapper
  // object is created for the parent node for each group of siblings so that
  // the parent’s dimensions are not discarded as we recurse. Since each group
  // of sibling was laid out in 1×1, we must rescale to fit using absolute
  // coordinates. This lets us use a viewport to zoom.
    
  function name(d) {
    return d.parent
        ? name(d.parent) + "/" + d.name
        : d.name;
  }
  
  function layout(d) {
    if (d._children) {
      // treemap nodes comes from the treemap set of functions as part of d3
      treemap.nodes({_children: d._children});
      d._children.forEach(function(c) {
        c.x = d.x + c.x * d.dx;
        c.y = d.y + c.y * d.dy;
        c.dx *= d.dx;
        c.dy *= d.dy;
        c.parent = d;
        // recursion
        layout(c);
      });
    }
  } 
    
//Clear current selected items
function clearSearch(){
    if($('.nameDir').hasClass('selectedNav')){
       $('.nameDir').removeClass('selectedNav');
    }
}  
    
//Search for the directory navigation
$( "#searchNavigation" ).change(function() {
    var searchVal = $(this).val();
    
    //If 'search' is set
    clearSearch();
    if (searchVal){
        $('[title*="/'+searchVal+'"]').each(function(){
            var arr = $(this).attr("title").split("/");
            if (arr[arr.length-1].search(searchVal)>-1){
                for (var count = 1; count<arr.length; count++){
                    var newString = arr[0];
                    for (var t=1;t<=count;t++){
                        newString += "/"+arr[t];
                    }
                    $('[title="'+newString+'"]').addClass("selectedNav");
                }
            }
        });
    }
}).keyup( function () {
    $(this).change();
});
    
// Gets information from where the current file is and 
// navigate through the treemap when the user opens it
function goThroughMap(levelToGo){
    var arr = levelToGo.split("/");
    var level = 0;

    var animateToLocation;
    
    animateToLocation = setInterval( function() 
    {
        oneLevelDown(level, arr);
        level++;
        if (level>(arr.length-2)){
            clearInterval(animateToLocation);
        }
    }, 1000);
}

function goThroughMap2(levelToGo){
    var arr = levelToGo.split("/");
    var level = 0;
    var animateToLocationUp;
    var animateToLocationDown;
    
    var currentDir = $(".grandparent text").text();
    var arr2 = currentDir.split("/");
    
    var different=0;
    var time=1;
    
    for (var x=0;x<arr2.length;x++){
        if(arr[x]!=arr2[x]){
            different++;
        }
    }
    
    if (different==1){
        time = 600;
    }else if (different==2){
        time=2000;
    }else if (different==3){
        time=4000;
    }else if (different>3){
        time = 500 * Math.pow(2,different);
    }
    
    if (different>0){
        animateToLocationUp = setInterval( function() 
        {
            oneLevelUp();
            different--;
            if (different<1){
                clearInterval(animateToLocationUp);
            }
        }, 350*different);
    }
    animateToLocationDown = setInterval( function() 
    {
        oneLevelDown(level, arr);
        level++;
        if (level>(arr.length-2)){
            clearInterval(animateToLocationDown);
        }
    }, time);
}

// Activate transition function of treemap according to previous function
function oneLevelDown(level, arr){
    $(".depth .children").each(function(indexIn){
        if(arr[level+1]==$(".depth .children:eq("+indexIn+") text").text()){
            this.classList.add("selected");
            
            d3.selectAll("g.selected").each(function(d, i) {
                var onClickFunc = d3.select(this).on("click");
                onClickFunc.apply(this, [d, i]);
            });
        }
        
    });    
}
    
function oneLevelUp(){
    d3.selectAll("g.grandparent").each(function(d, i) {
        var onClickFunc = d3.select(this).on("click");
        onClickFunc.apply(this, [d, i]);
    });   
}

function removeMetas( str ) {
    return str.replace(/["~!#$%^&*\(\)+=`{}\[\]\|\\:;'<>,.\/?"\-]+/g, ''); //@ _ \t\r\n
}
    
// loadDirectoryNavigationMap
var skipTo;
function generateMap(i, arr, d){
    var foundIn = false;

    //FASTEST SO FAR
    //Find if the level was already created
    // $("#level1 #"+skipTo+" h"+(i+1)+"~div[id='"+arr[i-1]+arr[i]+"']").each(function(){
    //     foundIn = true;
    // });

    if ($("#level1 #"+skipTo+" h"+(i+1)+"~div[id='"+arr[i-1]+arr[i]+"']").length) {
      foundIn = true;
    }

    if (!foundIn && arr[i]!==undefined){
        if (i!=arr.length){
            $("#level1 h"+i+"~div:last").append("<li><h"+(i+1)+" title='"+d+"' onclick='goThroughMap2(this.title)' class='nameDir'>"+arr[i]+"</h"+(i+1)+"><div id='"+arr[i-1]+arr[i]+"'></div></li>");
        }
        else{
            $("#level1 h"+i+"~div:last").append("<li><h"+(i+1)+" title='"+d+"' onclick='goThroughMap2(this.title)' class='nameDir'>"+arr[i]+"</h"+(i+1)+"></li>");
        }
    }
}
    
function loadTreeMap(d){

    //Go through hierarchy
    var fullName = name(d);
    if (d._children) {
        d._children.forEach(function(c) {
            var arr = fullName.split("/");
            var found = false;
            
            if (skipTo != arr[1]){
                $(".treeMapNavigation #level1 h2 span").each(function(){
                    if ($(this).text() == arr[1]){
                        found = true;
                        skipTo = arr[1];
                    }
                });
                if ((!found) && arr[1]!==undefined){
                    $(".treeMapNavigation #level1").append("<li><h2 title='"+fullName+"' onclick='goThroughMap2(this.title)' class='nameDir'><span>"+arr[1]+"</span></h2><div id='"+arr[1]+"'></div></li>");} 
                //<a href='#'><i class='fa fa-code'></i></a>
            }
            
            for (var i=2; i<arr.length;i++){
                generateMap(i, arr, fullName);
            }
            loadTreeMap(c);
        });
    }
}

function buildNestedList(treeNodes, rootId) {
  var nodesByParent = {};

  $.each(treeNodes, function(i, node) {
    if (!(node.parent in nodesByParent)) nodesByParent[node.parent] = [];
    nodesByParent[node.parent].push(node);
  });

  function buildTree(children) {
    var $container = $("<ul>");

    if (!children) return;
    $.each(children, function(i, child) {
      $("<li>", {text: child.name})
      .appendTo($container)
      .append( buildTree(nodesByParent[child.id]) );
    });
    return $container;
  }
  return buildTree(nodesByParent[rootId]);
}
    
function fixLegend(){
    $(".legendCells .cell").each(function(indexIn){
        var x = 32*indexIn;
        $(this).attr("transform", "translate("+x+",0)");
        $(".legendCells .cell:eq("+indexIn+") .label").attr("transform", "translate(15,33)");
    });
}

// Fix URL dynamically by removing the end of the current 
// and using the first part of it
function removeLastPartOfDirectory(url){
    if (url.indexOf("/visualizeCode.php?")>-1){
      var newURL = url.split('/visualizeCode.php?');//  
    }
    else{
      var newURL = url.split('/');  
    }
    newURL.pop();
    return( newURL.join('/') );
}
var urlIn = removeLastPartOfDirectory(window.location.href);

// /Applications/XAMPP/xamppfiles/htdocs/slicing/vizSlice  
// /slicing/vizSlice

var file = urlIn + "/json_files/" + <?php echo json_encode($_SESSION['filename']);?> + ".json";
var xmlFile = urlIn + "/xml_files/" + <?php echo json_encode($_SESSION['filename']);?> + ".xml";

d3.json(file, function(root) {
  initialize(root[0]);
  accumulate(root[0]);
  layout(root[0]);
  var data = getSample(root[0]);
  var stats = root[1];
  console.log(stats);
  //console.log(data);
  color = d3.scale.linear()
        .domain(stats)
        .range(["blue","green","yellow","orange","red"]);
  display(root[0]);
    
  var linear = color;

var leg = d3.select("#legend").append("svg");

leg.append("g")
  .attr("class", "legendLinear")
  .attr("transform", "translate(20,20)");

var legendLinear = d3.legend.color()
  .shapeWidth(30)
  .cells(stats)
  .orient('horizontal')
  .scale(linear);
leg.select(".legendLinear")
  .call(legendLinear);

  function display(d) {
    
  <?php
    if (strcmp($currentFile,"navigation.php")==0){
        echo "document.getElementById('directory').innerHTML = name(d);";
    }
  ?>  
    
    grandparent
        .datum(d.parent)
        .on("click", transition)
        .select("text")
        .text(name(d))

    // color header based on grandparent's value
    grandparent
      .datum(d.parent)
      .select("rect")
      .attr("fill", function(){return color(d.avg)})
      //.attr("class", "grandparentRect")

    var g1 = svg.insert("g", ".grandparent")
        .datum(d)
        .attr("class", "depth");

    var g = g1.selectAll("g")
        .data(d._children)
      .enter().append("g");

    g.filter(function(d) {return d._children; })
        .classed("children", true)
        .on("click", transition);

    g.filter(function(d){return !d._children;})
        .on("click", function(d){
          var path = d.parent.name;
          var tmp = d.parent;
          while (tmp.parent){
            tmp = tmp.parent;
            path = tmp.name + "/" + path;            
          }

          window.location= removeLastPartOfDirectory(window.location.href) + 
                           "/visualizeCode.php?path=" + path + "&data=" + 
                           file + "&xml=" + xmlFile});
      
    g.selectAll(".child")
        .data(function(d) {
          return d._children || [d]; })
      .enter().append("rect")
        .attr("class", "child")
        .call(rect);

    g.append("rect")
        .attr("class", "parent")
        .call(rect)
      .append("title")
        .text(function(d) {
          if(d._children){
            return d.name + ', Average Slice Size: ' + d.avg;
          } else {
            return d.name + ', Number of Slices: ' + d.value;
          }});

    g.append("text")
        .attr("dy", ".75em")
        .text(function(d) { return d.name; })
        .call(text);

    function transition(d) {
      if (transitioning || !d) return;
      transitioning = true;

      var g2 = display(d),
          t1 = g1.transition().duration(500),//750
          t2 = g2.transition().duration(500);//750

      // Update the domain only after entering new elements.
      x.domain([d.x, d.x + d.dx]);
      y.domain([d.y, d.y + d.dy]);

      // Enable anti-aliasing during the transition.
      svg.style("shape-rendering", null);

      // Draw child nodes on top of parent nodes.
      svg.selectAll(".depth").sort(function(a, b) { return a.depth - b.depth; });

      // Fade-in entering text.
      g2.selectAll("text").style("fill-opacity", 0);

      // Transition to the new view.
      t1.selectAll("text").call(text).style("fill-opacity", 0);
      t2.selectAll("text").call(text).style("fill-opacity", 1);
      t1.selectAll("rect").call(rect);
      t2.selectAll("rect").call(rect);

      // Remove the old node when the transition is finished.
      t1.remove().each("end", function() {
        svg.style("shape-rendering", "crispEdges");
        transitioning = false;
      });
    }
    // console.log(g);
    return g;
  }

  function text(text) {
    text.attr("x", function(d) { return x(d.x) + 6; })
        .attr("y", function(d) { return y(d.y) + 6; })
        .attr("fill", function (d) {return "#000000"});
  }

  function rect(rect) {
    rect.attr("x", function(d) { return x(d.x); })
        .attr("y", function(d) { return y(d.y); })
        .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
        .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); })
        .attr("fill", function(d){
          if(d._children && typeof d._children !== "undefined"){
            return color(d.avg);
          } else {
            return color(d.value);
          }});
  }
    
  //return names
  function name(d) {
    return d.parent
        ? name(d.parent) + "/" + d.name
        : d.name;
  }
    
  <?php
    if (strcmp($currentFile,"navigation.php")==0){
        //Load files into treemap navigation
        //echo "loadTreeMap(root[0]);";
    }
  ?>
  $('#loading').modal('hide');

}); 
</script>