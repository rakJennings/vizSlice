<?php
    $currentFile = end((explode('/', $_SERVER['PHP_SELF'])));
?>

<!-- Sidebar -->
    <div id="sidebar-wrapper2">
        <ul class="sidebar-nav2">
            <li>
                <a href="index.php" 
                   <?php
                        if (strcmp($currentFile,"index.php")==0){
                            echo "class='active'";
                        }
                   ?>
                ><i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <?php
                if (strcmp($currentFile,"navigation.php")==0){ // || strcmp($currentFile,"visualizeCode.php")==0
                    echo "<li><a href='#menu-toggle' id='menu-toggle'><i class='glyphicon glyphicon-menu-hamburger'></i>Navigation</a></li>";
                }
                //else if (strcmp($currentFile,"visualizeCode.php")!=0){
                //    echo "<li><a href='navigation.php'><i class='fa fa-file-code-o'></i>Slice Code</a></li>";
                //}
            ?>
            
            <?php
                if (strcmp($currentFile,"visualizeCode.php")==0){
                    echo "<li><a href='#' data-toggle='modal' data-target='#preferences'><i class='fa fa-edit'></i>Preferences</a></li>";
                }
            ?>
            
            <?php
                if (strcmp($currentFile,"visualizeCode.php")==0){
                    echo "<li><a href='#' data-toggle='modal' data-target='#treeMap'><i class='fa fa-map'></i>Tree map</a></li>";
                }
            ?>
            
            <!-- <li>
                <a href="profile.php"
                   <?php
                        // if (strcmp($currentFile,"profile.php")==0){
                        //     echo "class='active'";
                        // }
                   ?>
                   ><i class="glyphicon glyphicon-user"></i> My Account</a>
            </li> -->
            
<!--
            <li>
                <a href="#"><i class="fa fa-github"></i>Projects</a>
            </li>
-->
            <!-- <li>
                <a href="#"><i class="glyphicon glyphicon-share-alt"></i>Share</a>
            </li> -->
            <li>
                <a href="contact.php"
                    <?php
                        if (strcmp($currentFile,"contact.php")==0){
                            echo "class='active'";
                        }
                   ?>   
                ><i class="glyphicon glyphicon-send"></i>Contact Us</a>
            </li>
            
            <!-- <li>
                <a href="#"><i class="glyphicon glyphicon-info-sign"></i>Info</a>
            </li> -->
        </ul>
    </div>
<!-- /#sidebar-wrapper -->

<!-- Sidebar -->
    <?php
        if (strcmp($currentFile,"navigation.php")==0){ // || strcmp($currentFile,"visualizeCode.php")==0
            echo '<div id="sidebar-wrapper" class="navigationTree">';
            
            //Search input
            echo '<div class="input-group margin-bottom-sm treeMapSearch">';
            echo '<input class="form-control search" placeholder="Search" id="searchNavigation">';
            echo '<span class="input-group-addon"><i class="fa fa-search fa-fw" aria-hidden="true"></i></span></div>';
            
            echo '<ul class="sidebar-nav treeMapNavigation" id="namesDir"> <li class="accordion">';
            echo "<h1>".$_SESSION["filename"]."</h1>";
            echo '<ul class="opened-for-codepen" id="level1">';
            echo '</ul></li></ul></div>';
        }
    ?>
<!-- /#sidebar-wrapper -->